module cdr-downloader

go 1.16

require (
	github.com/go-redis/redis/v8 v8.9.0
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
)
