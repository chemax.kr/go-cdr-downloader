package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/go-redis/redis/v8"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
)

var log = logrus.New()

var ourIps []string      //Наши адреса (отсекаем входящие)
var blockedIps []string  //Адреса, звонки на которые не обрабатываются
var concurrency int      //Число потоков для скачивания файлов на проверку
var WavFolder string     //Куда качаем файлы на проверку
var TmpFolder string     //Куда складываем рабочие версии файлов на проверку(без кпв, моно)
var UrlForWavFile string //Откуда забираем файл (ссылка на voipmon API)
var UrlForCdr string     //Откуда забираем файл (ссылка на voipmon API)
var checkVoskPath string //Скрипт для проверки файла
var checkPath string     //Скрипт для проверки файла
var login string
var password string
var RemoveMainAudioFile string
var StorePcap string //Скачивать ли pcap

var Cookie string //кукис, "PHPSESSID=96fpb2ng6oimtqoampb2nk7nr5"

var redisPath = "localhost:6379" //localhost:6379
var redisDB = 0                  //0
var redisPass = ""               // empty

var ctx = context.Background()
var rdb *redis.Client

func init() {
	log.Out = os.Stdout
	log.SetLevel(logrus.DebugLevel)
	log.SetFormatter(&logrus.JSONFormatter{})
	file, err := os.OpenFile("cdr-downloader.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		log.Out = file
	} else {
		log.Info("Failed to log to file, using default stderr")
	}
	err = godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	} else {
		if os.Getenv("StorePcap") != "" {
			StorePcap = os.Getenv("StorePcap")
		}
		if os.Getenv("RemoveMainAudioFile") != "" {
			RemoveMainAudioFile = os.Getenv("RemoveMainAudioFile")
		}
		if os.Getenv("Login") != "" {
			login = os.Getenv("Login")
		} else {
			panic("Please, setup the Login as ENV variable")
		}
		if os.Getenv("Password") != "" {
			password = os.Getenv("Password")
		} else {
			panic("Please, setup the Password as ENV variable")
		}
		if os.Getenv("concurrency") != "" {
			concurrency, _ = strconv.Atoi(os.Getenv("concurrency"))
		} else {
			concurrency = 2
		}
		if os.Getenv("WavFolder") != "" {
			WavFolder = os.Getenv("WavFolder")
		} else {
			WavFolder = "/tmp"
		}
		if os.Getenv("TmpFolder") != "" {
			TmpFolder = os.Getenv("TmpFolder")
		} else {
			TmpFolder = "/tmp"
		}
		if os.Getenv("UrlForWavFile") != "" {
			UrlForWavFile = os.Getenv("UrlForWavFile")
		} else {
			panic("Please, setup the UrlForWavFile as ENV variable")
		}
		if os.Getenv("UrlForCdr") != "" {
			UrlForCdr = os.Getenv("UrlForCdr")
		} else {
			panic("Please, setup the UrlForCdr as ENV variable")
		}
		if os.Getenv("checkPath") != "" {
			checkPath = os.Getenv("checkPath")
		} else {
			panic("Please, setup the checkPath as ENV variable")
		}
		if os.Getenv("checkVoskPath") != "" {
			checkVoskPath = os.Getenv("checkVoskPath")
		}
		if os.Getenv("redisPath") != "" {
			redisPath = os.Getenv("redisPath")
		}
		if os.Getenv("redisDB") != "" {
			redisDB, _ = strconv.Atoi(os.Getenv("redisDB"))
		}
		if os.Getenv("redisPass") != "" {
			redisPass = os.Getenv("redisPass")
		}
		if os.Getenv("blockedIps") != "" {
			blockedIps = strings.Split(os.Getenv("blockedIps"), ",")
		}
		if os.Getenv("ourIps") != "" {
			ourIps = strings.Split(os.Getenv("ourIps"), ",")
		}
	}
}

func main() {
	fmt.Print("version with VOSKv2")
	log.Warn("version with VOSKv2")
	getPHPSESSID()
	rdb = redis.NewClient(&redis.Options{
		Addr:     redisPath,
		Password: redisPass, // no password set
		DB:       redisDB,   // use default DB
	})
	go SearchForTasks()
	Download()
	//http.Handle("/count", new(countHandler))
	log.Fatal(http.ListenAndServe(":8080", nil))
}
