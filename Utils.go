package main

import (
	"encoding/json"

	"net/http"
	"net/url"
	"os"
)

func getPHPSESSID() {
	login := os.Getenv("Login")
	password := os.Getenv("Password")
	CookieUrl := os.Getenv("CookieURL")
	params := "module=bypass_login" +
		"&user=" + url.QueryEscape(login) +
		"&pass=" + url.QueryEscape(password)
	path := CookieUrl + "?" + params
	resp, err := http.Get(path)
	defer resp.Body.Close()
	if err != nil {
		log.Fatal(err)
	} else {
		var res map[string]interface{}
		json.NewDecoder(resp.Body).Decode(&res)
		if !res["success"].(bool) {
			panic(res["msg"].(string))
		} else {
			Cookie = "PHPSESSID=" + res["SID"].(string)
		}
	}
}
