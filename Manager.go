package main

import (
	"strconv"
	"sync"
	"time"
)

const DownloaderConcurrency = 0

func SearchForTasks() {
	wg := &sync.WaitGroup{}
	wg.Add(DownloaderConcurrency)
	//DownloaderResults := make(chan string)
	DownloaderTasks := make(chan string)
	go func() {
		for {
			record, _ := rdb.LPop(ctx, "DownloaderTasks").Result()
			if record != "" {
				DownloaderTasks <- record
			} else {
				time.Sleep(time.Second)
			}
		}
	}()
	go func() {
		defer wg.Done()

		for t := range DownloaderTasks {

			task, _ := rdb.HGetAll(ctx, t).Result()
			lt, _ := strconv.Atoi(task["fdurationgt"])
			gt, _ := strconv.Atoi(task["fdurationlt"])
			gt += lt

			d := CDRGrabber{
				task["fdatefrom"],
				task["fdateto"],
				task["fdurationgt"],
				strconv.Itoa(gt),
				Provider{"", ""},
			}
			log.Info("Download task", d)
			if task["ip"] != "" {
				d.provider.ip = task["ip"]
			}
			if task["provname"] != "" {
				d.provider.name = task["provname"]
			}
			d.start()
		}
	}()
}
