package main

import (
	"os"
	"os/exec"
	"strconv"
)

func removeKpvOld(filename string) bool {
	kpvDur, _ := rdb.Get(ctx, filename+"_kpvDur").Result()
	cmd := "ffmpeg"
	args := []string{
		// "-hide_banner",
		// "-loglevel", "debug",
		"-i", TmpFolder + "/right_" + filename + ".wav",
		"-ss", kpvDur,
		"-ar 16000",
		TmpFolder + "/right2_" + filename + ".wav",
	}
	result := exec.Command(cmd, args...)
	output, err := result.Output()

	log.Debugf("remove kpv %s sec from %s", kpvDur, filename)
	if err != nil {
		log.Errorf("remove kpv %s sec from %s", kpvDur, filename)
		log.Error(err)
		log.Error(output)
		log.Error(cmd, args)
	}
	return err == nil
}

func removeKpv(filename string) bool {
	kpvDur, _ := rdb.Get(ctx, filename+"_kpvDur").Result()
	cmd := "sh"
	args := []string{
		"-c",
		"ffmpeg -hide_banner -loglevel debug -i " +
			TmpFolder + "/right_" + filename + ".wav -ss " + kpvDur + " " +
			TmpFolder + "/right2_" + filename + ".wav",
	}
	args2 := []string{
		"-c",
		"ffmpeg -hide_banner -loglevel debug -i " +
			TmpFolder + "/right_" + filename + ".wav -ss " + kpvDur + " -ar 16000 " +
			TmpFolder + "/right3_" + filename + ".wav",
	}
	result2 := exec.Command(cmd, args2...)
	result := exec.Command(cmd, args...)
	output2, err2 := result2.Output()
	output, err := result.Output()

	log.Debugf("remove kpv %s sec from %s", kpvDur, filename)
	if err2 != nil {

		log.Errorf("remove kpv %s sec from %s", kpvDur, filename)
		log.Error(err2)
		log.Error(output2)
		log.Error(cmd, args2)
	}
	if err != nil {
		log.Errorf("remove kpv %s sec from %s", kpvDur, filename)
		log.Error(err)
		log.Error(output)
		log.Error(cmd, args)
	}
	return err == nil
}

func checkFileByVosk(filename string) bool {
	log.Debugf("vosk path: %s filename: %s", checkVoskPath, filename)
	if checkVoskPath != "" {
		cmd := "/usr/bin/python3"
		args := []string{
			checkVoskPath,
			filename,
		}
		_, err := exec.Command(cmd, args...).Output()
		log.Info("MOD_VOSK")
		// log.Info(Output)
		return err == nil
	} else {
		log.Debug("MOD_VOSK path not found")
	}
	return false
}

func checkFile(filename string) bool {
	cmd := "/usr/bin/python3"
	args := []string{
		checkPath,
		filename,
	}
	_, err := exec.Command(cmd, args...).Output()
	return err == nil
}

func splitFile(filename string) error {
	cmd := "/usr/bin/ffmpeg"
	args := []string{
		"-hide_banner",
		"-loglevel", "panic",
		"-i",
		WavFolder + "/" + filename + ".wav",
		"-map_channel", "0.0.1",
		TmpFolder + "/right_" + filename + ".wav",
	}
	_, err := exec.Command(cmd, args...).Output()
	if err != nil {
		return err
	}
	return nil
}

func ProcessFile(filename string) {

	if _, err := os.Stat(WavFolder + "/" + filename + ".wav"); err == nil {
		log.Debug(WavFolder + "/" + filename + ".wav exist, processing")
	} else {
		log.Warn(WavFolder + "/" + filename + ".wav NOT exist")
		return
	}
	_ = os.Remove(TmpFolder + "/right_" + filename + ".wav")

	err := splitFile(filename)
	if err != nil {
		log.Error(err)
	} else {
		log.Debugf("split file %s success", filename)
	}

	if removeKpv(filename) {
		log.Debug("KPV for ", filename, "removed successfully")
		if !checkFileByVosk(filename) && checkVoskPath != "" {
			log.Errorf("error check file %s with Vosk", filename)
		}
		conDur, _ := rdb.Get(ctx, filename+"_conDur").Result()
		conDurInt, _ := strconv.Atoi(conDur)
		if conDurInt > 60 {
			if checkFile(filename) {
				log.Debug("check file", filename, "successfully")
			}
		}
		if StorePcap == "true" {
			go func() {
				err := GetPcap(filename)
				if err != nil {
					log.Error(filename, err)
				}
			}()
		}
		if RemoveMainAudioFile == "true" {
			_ = os.Remove(WavFolder + "/" + filename + ".wav")
		}
		_ = os.Remove(TmpFolder + "/right_" + filename + ".wav")
		_ = os.Remove(TmpFolder + "/right2_" + filename + ".wav")
		_ = os.Remove(TmpFolder + "/right3_" + filename + ".wav")
	}
}
