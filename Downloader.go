package main

import (
	"errors"
	"io"
	"net/http"
	"net/url"
	"os"
	"sync"
	"time"
)

func CheckHeader(header string) bool {
	return header == "audio/x-wav"
}

func GetPcap(id string) error {
	params := "task=getPCAP" +
		"&user=" + url.QueryEscape(login) +
		"&password=" + url.QueryEscape(password) +
		"&params=" + url.QueryEscape("{\"cdrId\": \""+id+"\",\"zip\":true}")
	path := UrlForWavFile + "?" + params
	resp, _ := http.Get(path)
	//application/octet-stream
	defer resp.Body.Close()
	 if resp.Header.Get("Content-Type") != "application/octet-stream"{
	 	log.Error(path)
	 	return errors.New("wrong content type")
	 }
	out, _ := os.Create(WavFolder + "/" + id + ".zip")
	defer out.Close()
	io.Copy(out, resp.Body)
	return nil
}

func GetCallRecords(id string) string {
	params := "task=getVoiceRecording" +
		"&user=" + url.QueryEscape(login) +
		"&password=" + url.QueryEscape(password) +
		"&params=" + url.QueryEscape("{\"cdrId\": \""+id+"\"}")
	path := UrlForWavFile + "?" + params
	log.Info(path)
	resp, _ := http.Get(path)
	//println(path)
	defer resp.Body.Close()
	if CheckHeader(resp.Header.Get("Content-Type")) {
		out, _ := os.Create(WavFolder + "/" + id + ".wav")
		defer out.Close()
		io.Copy(out, resp.Body)
		return id
	} else {
		log.Warn("Not audio record " + id)
	}
	return ""
}

func Download() {
	wg := &sync.WaitGroup{}
	wg2 := &sync.WaitGroup{}
	wg2.Add(concurrency)
	wg.Add(concurrency)
	results := make(chan string)
	tasks := make(chan string)

	go func() {
		wg.Wait()
		//close(results)
	}()
	go func() {
		for {
			record, _ := rdb.LPop(ctx, "records").Result()
			if record != "" {
				tasks <- record
			} else {
				time.Sleep(time.Second)
			}
		}
	}()

	for i := 1; i <= concurrency; i++ {
		go func(id int) {
			defer wg.Done()

			for t := range tasks {
				log.Info("worker", id, "processing job", t)
				res := GetCallRecords(t)
				if res != "" {
					results <- res
				}

			}
		}(i)
	}
	for z := 1; z <= concurrency; z++ {
		go func(id int) {
			defer wg2.Done()
			for r := range results {
				ProcessFile(r)
			}
		}(z)
	}
}
