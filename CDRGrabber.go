package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"net/http"
	"net/url"
	"strings"
)

type Provider struct {
	ip   string
	name string
}

type CDRGrabber struct {
	fdatefrom   string
	fdateto     string
	fdurationgt string
	fdurationlt string
	provider    Provider
}

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func WriteCallToDb(call map[string]interface{}) {

	id := call["ID"].(string)
	sipcalledip, _ := call["sipcalledip"].(string)
	connDur, _ := strconv.Atoi(call["_connect_duration"].(string))
	itemJson, err := json.Marshal(call)
	dur, _ := strconv.Atoi(call["_duration"].(string))
	called, _ := call["called"].(string)
	calldate, _ := call["calldate"].(string)
	kpvDur := dur - connDur
	if err != nil {
		log.Error(err)
	}

	log.Debug(string(itemJson))

	rdb.Set(ctx, id+"full", itemJson, time.Hour*24*7)
	rdb.Set(ctx, id+"_kpvDur", kpvDur, time.Hour*48)
	rdb.Set(ctx, id+"_conDur", connDur, time.Hour*48)
	rdb.Set(ctx, id+"_called", called, time.Hour*48)
	rdb.Set(ctx, id+"_calldate", calldate, time.Hour*48)
	rdb.Set(ctx, id+"_ip", sipcalledip, time.Hour*48)
	rdb.RPush(ctx, "records", id)
}

func CDRProcessing(call map[string]interface{}, gt int) {
	sipcalledip, _ := call["sipcalledip"].(string)
	sipcallerip, _ := call["sipcallerip"].(string)
	if Contains(blockedIps, sipcalledip) || Contains(blockedIps, sipcallerip) {
		log.Debug("blocked IP " + sipcallerip + " -> " + sipcalledip)
	} else if Contains(ourIps, sipcalledip) {
		log.Debug("Our IP! ", sipcallerip,
			" -> ", sipcalledip)
	} else {
		connDur, _ := strconv.Atoi(call["_connect_duration"].(string))
		if connDur > gt {
			WriteCallToDb(call)
		}
	}
}

func (d CDRGrabber) start() {
	form := url.Values{
		"task":        {"LISTING"},
		"module":      {"CDR"},
		"fdatefrom":   {d.fdatefrom},
		"fdateto":     {d.fdateto},
		"fdurationgt": {d.fdurationgt},
		"fdurationlt": {d.fdurationlt},
	}
	gt, _ := strconv.Atoi(d.fdurationgt)
	if d.provider.ip != "" {
		form.Add("fsipcallerdip_type", "1")
		form.Add("fsipcalledip", d.provider.ip)
	}

	req, _ := http.NewRequest("POST", UrlForCdr, strings.NewReader(form.Encode()))
	if req != nil {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		req.Header.Add("Cookie", Cookie)
	}
	log.Infof("Request CDR list %s - %s", d.fdatefrom, d.fdateto)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Error("cdr request error:")
		log.Error(err)
	} else {
		if resp.StatusCode != 200 {
			log.Errorf("Status code %d", resp.StatusCode)
			return
		}
		var res map[string]interface{}
		err := json.NewDecoder(resp.Body).Decode(&res)
		if err != nil {
			log.Error("JSON CDR pars error: ", err)
		} else {
			if res["results"] != nil {
				for _, item := range res["results"].([]interface{}) {
					if item.(map[string]interface{})["_connect_duration"] != nil {
						CDRProcessing(item.(map[string]interface{}), gt)
					}
				}
			} else {
				log.Infof("No results for period from %s to %s", d.fdatefrom, d.fdateto)
				rdb.RPush(ctx, "StatusCheck",
					fmt.Sprintf("No results for period from %s to %s", d.fdatefrom, d.fdateto),
				)
			}
		}
	}
}
